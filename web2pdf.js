// index.js
const puppeteer = require("puppeteer");
const yargs = require('yargs');

const argv = yargs
  .scriptName("web2pdf.js")
  .usage('$0 --url <url> [--count number]')
  .option('url', {
    alias: 'u',
    nargs: 1,
    description: 'Url to save as pdf'
  })
  .demandOption(['url'])

  .option('count', {
    alias: 'c',
    default: 1,
    description: 'The number of pdf files to save.',
    type: 'integer'
  })

  .option('verbose', {
    alias: 'v',
    type: 'boolean',
    description: 'Be verbose about progres.'
  })
  .help()
  .alias('help', 'h').argv;


(async (url, count, verbose) => {
  const browser = await puppeteer.launch({
    executablePath: '/snap/bin/chromium'
  });
  const page = await browser.newPage();
  for (let i = 0; i < count; ++i) {
    await page.goto(url, {
      waitUntil: "networkidle0"
    });
    await page.evaluateHandle('document.fonts.ready');
    await page.setViewport({
      width: 1680,
      height: 1050
    });
    const title = await page.title();
    const filename = `${title.replace(' ', '_')}_${i}.pdf`;
    await page.pdf({
      path: filename,
      format: "A4",
      margin: {top: '1.5cm', right: '1cm', bottom: '2cm', left: '1cm'},
      printBackground: true
    });
    if (verbose) { console.log(`${filename} saved.`); }
  }

  await browser.close();
})(argv.url, argv.count, argv.verbose);
